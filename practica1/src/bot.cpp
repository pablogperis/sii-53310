#include <stdlib.h>
#include <stdio.h>


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include "DatosMemCompartida.h"

int main(){

	int fd_map = open("/tmp/fbot", O_RDWR|O_CREAT, 0600);
	ftruncate(fd_map, sizeof(DatosMemCompartida));

	DatosMemCompartida* md;
	if (fd_map == -1){
		perror("fbot");
		return 1;

	}
	md = static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ | PROT_WRITE, MAP_SHARED, fd_map, 0));
	close(fd_map);
	//*md = nada;
	if (md == (void *) -1){
		perror("bot mmap");
		return 1;
	}
	md->signal = 1;
	while(md->signal == 1){
		if (md->jugador.y2 < md->esfera.centro.y){
			md->accion = 1;
		}
		else if (md->jugador.y1 > md->esfera.centro.y)
			md->accion = -1;
		usleep(25000);
	}

	munmap(md, sizeof(DatosMemCompartida));
	return 0;
}
