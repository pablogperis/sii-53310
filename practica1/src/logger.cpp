//LOGGER

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

int main(){
	int fd;
	int player[2]; //player[0] = jugador; player[1] = puntos
	int rdbytes;

	while (mkfifo("/tmp/loggerfifo", 0600)){ // == -1 o != 0
		if (errno != EEXIST){ //si ya existe no pasa nada
			perror("FIFO creation");
			return 1;
		}
		unlink("/tmp/loggerfifo");
	}
	fd = open("/tmp/loggerfifo", O_RDONLY);
	if(fd == -1){
		perror("FIFO opening");
		return 2;
	}
	printf("\n");
	while(rdbytes = read(fd, &player, 2*sizeof(int))){ //chequear
		
		if (rdbytes != 2*sizeof(int)){
			perror("Lectura de FIFO en logger");
			return 2;
		}		

		printf("Jugador %d marca 1 punto, lleva un total de %d puntos\n", player[0], player[1]);

		
		
		
		if (player[1] == 3){
			close(fd);
			printf("t\tJugador %d ganador\n\t\tENHORABUENA\n", player[0]);
			//sleep(1);
			unlink("/tmp/loggerfifo");
			return 0;
		}
	}
	unlink("/tmp/loggerfifo");
	return 1;
}

