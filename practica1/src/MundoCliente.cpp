// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <vector>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include "glut.h"

using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	md->signal = 0;
	munmap(md, sizeof(DatosMemCompartida));
	close(fd_sac);
	close(fd_cas);
	unlink("/tmp/fbot");
	unlink("/tmp/casfifo");
	unlink("/tmp/sacfifo");
	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	for(int i = 0; i < esferas.size(); i++)
		esferas[i].Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	//static Esfera e;
	//static float actime;
	//static int player[2]; //DATOS A COMPARTIR CON LOGGER player[0] = jugador; player[1] = puntos 
	//int i;
	//int j;
	char cad[200];
	
	if (md != NULL){
		if (md->accion == -1)
			OnKeyboardDown('l', 0, 0);
		else if (md->accion == 1)
			OnKeyboardDown('o', 0, 0);
	}
	
	read(fd_sac, cad, sizeof(cad));
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &esferas[0].centro.x, &esferas[0].centro.y,  &jugador1.x1, &jugador1.y1, &jugador1.x2, &jugador1.y2, &jugador2.x1, &jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2);
	
	/*
	jugador1.Mueve(0.025f);

	if (md != NULL){
		jugador2.velocidad.y = 4 * md->accion;
	}

	jugador2.Mueve(0.025f);
	for(i = 0; i < esferas.size(); i++)
		esferas[i].Mueve(0.025f);
	
	for(i=0;i<paredes.size();i++)
	{
		for(int j = 0; j < esferas.size(); j++)
			paredes[i].Rebota(esferas[j]);
			paredes[i].Rebota(jugador1);
			paredes[i].Rebota(jugador2);
	}
	
	for(int i = 0; i < esferas.size(); i++){
		jugador1.Rebota(esferas[i]);
		jugador2.Rebota(esferas[i]);
	}
	
	for(int i = 0; i < esferas.size(); i++)
	{
		if(fondo_izq.Rebota(esferas[i]))
		{
			esferas.clear();
			puntos2++;
			

			player[0] = 2;
			player[1] = puntos2;
			write(fd_fifo, &player, 2*sizeof(int));

			
			e.centro.y=rand()/(float)RAND_MAX;
			e.velocidad.x=-2-2*rand()/(float)RAND_MAX;
			e.velocidad.y=-2-2*rand()/(float)RAND_MAX;
			esferas.push_back(e);
			actime = 0;

			return;
		}
	
		if(fondo_dcho.Rebota(esferas[0]))
		{
			esferas.clear();
			puntos1++;
		


			player[0] = 1;
			player[1] = puntos1;
			write(fd_fifo, &player, 2*sizeof(int));

			
			e.centro.y=rand()/(float)RAND_MAX;
			e.velocidad.x=-2-2*rand()/(float)RAND_MAX;
			e.velocidad.y=-2-2*rand()/(float)RAND_MAX;
			esferas.push_back(e);
			actime = 0;
			return;
		}
	}*/
	/*
	//para que coja la esfera más a la derecha

	actime = actime + 0.025;
	if (actime >= 10 && esferas.size() < 5){
		actime = 0;
		e.centro.y=rand()/(float)RAND_MAX;
		e.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		e.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		esferas.push_back(e);
	}

	j = 0;
	for (int i = 1; i < esferas.size(); i++){
		if (esferas[i].centro.x > esferas[j].centro.x)
			j = i;
	}*/

	md->jugador = jugador2;
	md->esfera = esferas[0];

	if (puntos1 == 3){
		exit(EXIT_SUCCESS);
	}
	if (puntos2 == 3){
		exit(EXIT_SUCCESS);
	}

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	unsigned char key2 = key;
	/*
	switch(key)
	{
	//	case 'a':jugador1.velocidad.x=-1;break;
	//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
	*/
	
	write(fd_cas, &key2, sizeof(key2));
}

void CMundo::Init()
{
	Plano p;
	Esfera e;
	
	

//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//esfera
	esferas.push_back(e);
	
	//MMAP CON BOT
	fd_map = open("/tmp/fbot", O_RDWR|O_CREAT, 0600);
	
	write(fd_map, &datos, sizeof(DatosMemCompartida));//también valdría ftruncate
	if (fd_map == -1)
		perror("[C] abriendo fbot");
		
	md = static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd_map, 0));
	if (md == (void *) -1){
		perror("[C] mmap");
		//exit(EXIT_FAILURE);
	}
	close(fd_map);
	
	//SERVIDOR A CLIENTE
	
	while (mkfifo("/tmp/sacfifo", 0600)){ // == -1 o != 0
		if (errno != EEXIST){ //si ya existe no pasa nada
			perror("[C] creando sacfifo ");
			exit(EXIT_FAILURE);
		}
		unlink("/tmp/sacfifo");
	}
	fd_sac = open("/tmp/sacfifo", O_RDONLY);
	if(fd_sac == -1){
		perror("[C] abriendo sacfifo");
		exit(EXIT_FAILURE);
	}
	
	//CLIENTE A SERVIDOR
	
	while (mkfifo("/tmp/casfifo", 0600)){ // == -1 o != 0
		if (errno != EEXIST){ //si ya existe no pasa nada
			perror("[C] creando casfifo ");
			exit(EXIT_FAILURE);
		}
		unlink("/tmp/casfifo");
	}
	fd_cas = open("/tmp/casfifo", O_WRONLY);
	if(fd_cas == -1){
		perror("[C] abriendo casfifo");
		exit(EXIT_FAILURE);
	}
	
	//sleep(5); 
}
